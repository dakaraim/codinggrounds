function [test,r,RR,R,p] = reshape_test(dim1,dim2,dim3)
% Script to validate the reshaping function gives desired output organizing
% subjects in a row wise fashion in increasing number of voxels.
% dim1: = features (time)
% dim2: = mesh points (voxels)
% dim3: = subjects
% test: first digit represents subject. 
%       second digit represents features
%       third digit represents voxel. 
% Structure One: Does first reshaping a matrix with a single digits to be 
%                [dim1 x dim3 x dim2] and then reshaping it to be 
%                [dim1 x (dim3*dim2)] equal a direct reshaping? Yes. 
% Structure Two: Repeat test one, but now keep track of subjects, voxels and time.
% Structure Three: permute test to work with transpose of multidimensional matrix
%                  [dim1 x dim3 x dim2].


%% Reshape function is suppoesd to preserve columwise order 
test = zeros(dim1, dim2, dim3);
for k = 1:dim3
    for j = 1:dim2
        test(:,j,k) = j*ones(dim1,1);
    end
end

r = struct('one',reshape(test, [dim1,dim3,dim2]));
RR = struct('one',reshape(r.one, [dim1,dim2*dim3])');
R = struct('one',reshape(test, [dim1,dim2*dim3])');
%   The above two reshapings seem to be equivalent. Need to improve the
%   indices of each column to see if the function preserves order across
%   columns and other dimensions

%% Second test to check if the subject, voxel, and time ordering are preserved 
% first digit is the subject number, the second digit is the voxel number
% and the third digit is the number of time points. 
test = zeros(dim1, dim2, dim3);
for k = 1:dim3
    for j = 1:dim2
        test(:,j,k) = [1:dim1] + j*10 + k*100;
    end
end
% switch dimensions two and three
r.two = reshape(test, [dim1,dim3,dim2]);
% reshape previous matrix to desired concatenation.
RR.two = reshape(r.two,[dim1,dim2*dim3])';
% In the future extend this to n-dimensions
R.two = reshape(test, [dim1,dim2*dim3])';



%% Third test: This test is different from the previous two in that now we 
%  want the second dimension along the rows and the first and thrid
%  dimensions concatennated. 
test = zeros(dim1, dim2, dim3);
for k = 1:dim3
    for j = 1:dim2
        test(:,j,k) = [1:dim1] + j*10 + k*100;
    end
end
% Reorder the indices 
p = permute(test,[2 1 3]);
r.three = reshape(p,[dim2,dim1*dim3])';

