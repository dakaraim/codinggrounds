% random variable to sum the number of  success in 7 bernoulli trials
function Y = SumSuccess(n,p,k)
for j = 1:n
    for i = 1:k
        x = inv_cdf(7,p);
        y(i) = sum(x);
    end
    Y(j) = sum(y);
    
    hist(Y)
end
end