function structural_master(smooth,search_dir,search_criteria,filename)
% This function is designed to take a raw T1 weighted image and to run it
% through the structural analysis up until its time to do statistics.

cd('/home/dakaraim/matlab/')
%% Set necessary directory paths
air_bin_dir = '/home/dakaraim/AIR5.3.0/bin';
% This is included in the ASSP directory
atlas_dir='/home/dakaraim/matlab/ASSP_default/master/assp_atlas/'; 
% modified cortical extraction script
cortical_extraction_assp ='/home/dakaraim/BrainSuite19a/bin/cortical_extraction_assp_1.sh'; 
svreg_bin = '/home/dakaraim/BrainSuite19a/svreg/bin/svreg.sh';
svreg_atlas = '/home/dakaraim/BrainSuite19a/svreg/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain';
pvc_dir = '/home/dakaraim/BrainSuite19a/svreg/bin/thicknessPVC.sh';
addpath(genpath(search_dir));

%% Inputs - Get all basenamess in an array
a = GetBaseNames(search_dir, search_criteria, filename);
a = a(1:end -1); % the last file line is empty.

%% Case statements of where to resume

%% ASSP bias correction and make BSE mask.
AsspBiasCorrect(a, air_bin_dir, atlas_dir);

%% Run cortical exraction with assp corrected files
% This funtion should check if the he.cortex.dewisp.mask.nii.gz files
% exist. If they do not exist, then quit. Otherwise continue the cortical
% extraction.
CorticalExtraction(a,cortical_extraction_assp);

%% Run svreg and use single threaded mode. See Documentation for other flags
%for i = 1:length(a)-1; B{i} = [a{i} , '.roiwise.stats.txt']; end

flags = ['U','m']; % add each flag independently. 
%svreg(a(~isfile(B)),svreg_bin, svreg_atlas,flags) % Re-run for subjects that didn't finish 
svreg(a,svreg_bin, svreg_atlas, flags)


%% Run cortical thickness measurements using partial volume fractionation
PVC_Thickness(a,pvc_dir);

%% Generate Stats file

addpath(genpath('/home/ajoshi/git_sandbox/svreg/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/3rdParty'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/MEX_Files'));

for i = 1:length(a)
    generate_stats_xls(a{i});
end

%% Smoothing - Run this routine j times to save desired level of smoothing.
% Performs spatial smoothing on the surface statistic
smooth_pvcThick(a,smooth);


%% Create longitudinal BIDS directory
unix('/home/dakaraim/Data/MH_adolescent_waves/HandEdited/BrainSuite_Wave2_Processed/bssr_files.sh');
unix('/home/dakaraim/Data/MH_adolescent_waves/HandEdited/wave1/move_file.sh');

%% Calculate longitudinal change in Jacobian Determinant
deformation_difference();

%% Run R stats 
unix('/home/dakaraim/R/Social-Emotions/BSSR_Long.R');

%% Invert Statistic for interpretation
input_nii = ' ';
output_nii = ' ';
negate_nii(input_nii, output_nii)

%% Multisubject viewer

end


