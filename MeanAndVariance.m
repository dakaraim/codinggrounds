function [x] = MeanAndVariance(com,x)
%% computes the mean  across subjects and variace of features (time) at every voxel
for i = 1:length(com)
    % find the number of times that voxel was sampled and save index
    x.sameind = find(x.vox == com(i)); 
    % use index to get subspace vector
    x.sample(1:length(x.sameind),:) = x.subspace(x.sameind,:);
    % compute the mean across subjects
    x.av(i,:) = mean(x.sample,1);
    % Compute square of distance from the mean in each voxel in each
    % subject
    for j = 1: size(x.sample,1)
        x.ss(j,:,:) = (x.sample(j,:) - x.av)'*(x.sample(j,:) - x.av);
    end
    % Compute sum of squares in each voxel
    x.var(i,:,:) = sum(x.ss,1);
end
