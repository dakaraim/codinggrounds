function Tsq = Hotelling_OneVox(x,y,k)
% write a section that looks at the arguments. If 3 arguments, do rank
% reduction, if only 2 arguments, then skip rank reduction.

% hotellings student T test for one voxel
% Commented section performs a rank reduction on the voxels

nx = size(x,2) ; ny = size(y,2);

%Joint Rank Projector
XY = [x y];

% Compute SVD and choose k first values to reduce the rank.
[Uxy,~,~] = svd(XY);

ProjX = (Uxy(1:k,:)*x);
ProjY = (Uxy(1:k,:)*y);


% Average over subjects
yBar = mean(ProjX,2); xBar = mean(ProjY,2);
%xBar = mean(x,2); yBar = mean(y,2);

% Compute population correlation matrix
SSx = cov(ProjX'); SSy = cov(ProjY');
%SSx = cov(x'); SSy = cov(y');

Spl = (SSx*(nx-1) + SSy*(ny-1))/(nx+ny-2);

% Compute Tsquared statistic at every voxel (1,V)
% Tsq = (xBar-yBar)'*Spl*(xBar-yBar);
Tsq = (xBar-yBar)'*((Spl*((1/nx)+(1/ny)))\(xBar-yBar));
end

