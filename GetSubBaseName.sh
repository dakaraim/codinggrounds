#!/bin/bash
# Function searches for files in directory $1 with string $2 and creates
# a list of base names saving the output in file $3
# INPUTS: $1 is the search directory
#         $2 is search criteria 
#         $3 is the output file either .txt or .csv

if [ -e "$3" ]
   echo "Using existing file "$3""
   exit
then
    list=$(find "$1" -maxdepth 3 -name "*$2*" -print | sort | cut -d'.' -f1)
    echo "$list" > "$3"
fi
