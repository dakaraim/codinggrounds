function [stat,df,x,y,XY] = Hotelling(x,y,k)

%% Hotellings T test simulation for two groups
% This code performs a rank reduction from p to k features (time),
% x and y need to be structures with data elements of size [V,T,n].
% INPUTS: x and y are structs, k is a scalar
% OUTPUTS: Tsq is scalar, Err is a struct that contains the error from a
%          reduced rank approximation. df is a struct that contains the
%          degres of freedom for the numerator and denominator as
%          referenced from an f-distribution

nx = size(x.data,3) ; ny = size(y.data,3); 
V = size(x.data,1); T = size(x.data,2);

if sum(isequal(squeeze(mean(x.data,2)), squeeze(mean(y.data,2))),2) ~= 0
   disp("Warning, Matrices have the same mean!")
end

% Allocate space for variables - No Need due to size of matrices

% Joint Rank Projector concatenate across columns(subject)
XY = struct('data',cat(3,x.data, y.data));

% Get x, y and joint matrix indices
ind = [1:V]';
x.ind = bsxfun(@times,ind,1:nx); x.ind = x.ind(:); 
y.ind = bsxfun(@times,ind,[1:ny] + nx); y.ind = y.ind(:);
XY.ind = bsxfun(@times,ind,1:(nx+ny)); XY.ind = XY.ind(:);

% Reshape matrix to be T x (V*(nx+ny))
XY.data = reshape(XY.data,[T,(nx+ny)*V])';

[~,~,XY.V] = svd(XY.data,'econ');

% Projections should be dependent upon original data. Choose k << L
% subject-voxels and compute projection into orthonormal basis of time.
% This should serve as dimensionality reduction.
x.data = XY.data(x.ind,:)*XY.V(:,1:k);
y.data = XY.data(y.ind,:)*XY.V(:,1:k);

% Compute mean in every voxel across subjects
x.data = permute(reshape(x.data', [k, V, nx]),[1 3 2]); x.av = mean(x.data,2);
y.data = permute(reshape(y.data', [k, V, ny]),[1 3 2]); y.av = mean(y.data,2);

% Compute time covariance matrix with outer product of time vector.
X_Xbar=bsxfun(@minus,x.data,x.av);
Y_Ybar=bsxfun(@minus,y.data,y.av);

% xCov = SumSquares(x.data,x.av,V,nx); 
% yCov = SumSquares(y.data,y.av,V,ny);

% find covariances (without averaging)
Wx = multilinear_prod (X_Xbar, permute (X_Xbar, [2 1 3]));
Wy = multilinear_prod (Y_Ybar, permute (Y_Ybar, [2 1 3]));
W=(Wx+Wy)/(nx+ny-2);
W = bsxfun (@plus, W, 1e-5 * eye (size (W, 1)));

% Degrees of freedom in terms of F statistic numerator and denominator
% degrees of freedom
df = struct('numerator',k);
df.denominator = nx + ny - 2; df.numerator = k;

% Loop over all the voxels.How to eliminate for loops here? 

% compute Hotelling's Tsquared test statistic

for i = 1:V
    % Pooled variance
    Spl = (squeeze(xCov(i,:,:))*(nx-1) + squeeze(yCov(i,:,:))*(ny-1))./df.denominator;
    % sample covariacnce matrix
    Spl = Spl*((1/nx)+(1/ny));
    stat(i) = (x.av(:,i)-y.av(:,i))'*(Spl\(x.av(:,i)-y.av(:,i)));
end

end