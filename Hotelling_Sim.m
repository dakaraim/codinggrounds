function [Tsq, Err, df,x,y] = Hotelling_Sim(x,y,L,k)
%% Hotellings T test simulation for two groups of randomly sampled voxles from a joint rank matrix. 
%  Compute Tsq statistic in voxels that have satisfied these two conditions.
%  1) The same voxel was not sampled in the same subject
%  2) The same voxel was sample more than once across subjects in a group
% This code performs a rank reduction from p to k features (time),
% x and y matrices shuoud be dimension [T,n,V]. 
% The simlation will perform the test  on L < V vertices. If the number of
% subjects that have the same sampled voxel is less than k, then L must
% increase such that the number of times each voxel was sampled acrosss
% subjects inreases. 

x.n = size(x.data,2) ; y.n = size(y.data,2); 
V = size(x.data,3); T = size(x.data,1);

% Joint Rank Projector concatenate across columns(subject)
XY = struct('data',[x.data y.data]);

% Reshape matrix to be T x (V*(nx+ny))
XY.flat = reshape(XY.data,[T,(x.n+y.n)*V])';
% Randomly sample subject vertices from (x.n + y.n) subjects. Indices are
% sampled from a discrete uniform distrubtion.
ind = randi(V*(x.n+y.n),[L,1]); % get L indices  
% Figure out which indices belowng to X and Y groups.
x.ind = ind(ind <= x.n*V); y.ind = ind(ind > x.n*V); 
% Call function to recover subject and voxel number
[x.vox,x.sub] = recoverSubsVoxels(x.ind,V);
[y.vox,y.sub] = recoverSubsVoxels(y.ind,V); y.sub = y.sub - x.n; % subtract offset 

% Check limiting conditions 1 and 2
x = CheckSampling(x); y = CheckSampling(y);

% get voxles that were sampled more than once and sampled in both groups
com = intersect(x.collect,y.collect);
com = com(com ~= 0);
% Compute SVD on joint rank matrix
[XY.U,XY.S,XY.V] = svd(XY.flat(ind,:),'econ');
% Project onto rank-reduced time series vector space
% Projections should be dependent upon original data. Choose k << L
% subject-voxels and compute projection into orthonormal basis of time.
% This should serve as dime nsionality reduction
x.subspace = XY.flat(x.ind,:)*XY.V(:,1:k);
y.subspace = XY.flat(y.ind,:)*XY.V(:,1:k);

% loop through each of the voxels 
x = MeanAndVariance(com,x);
y = MeanAndVariance(com,x);
df = struct('numerator',k);
df.denominator = length(com); df.numerator = k;
for  i = 1:length(com)
    % Pooled variance
    Spl = (squeeze(x.var(i,:,:))*(length(find(x.vox == com(i)))-1) + squeeze(y.var(i,:,:))*(length(find(y.vox == com(i)))-1))./df.denominator;
    % sample covariacnce matrix
    Spl = Spl*((1/length(find(x.vox == com(i))))+(1/length(find(y.vox == com(i)))));
    Tsq(i) = (x.av(i,:)-y.av(i,:))*(Spl\(x.av(i,:)-y.av(i,:))');
end
%{
% Count how many times each voxel was randomly sampled.
[ax,ex] = histcounts(x.vox,unique(x.vox));
% Save voxels that were sampled n times.
for n = 1:max(ax)
    sample{n} = ex(ax == n);
    % Save index of sampled voxels in x.ind
    for i = 1:size(sample{n})
        SplVox{n,i} = x.ind(x.vox == sample{n}(i));
    end    
end

for n = 1:max(ax)
    for i = 1:size(sample{n})
        for j = 1:size(SplVox{n,i})
            % Fails when a voxel was sample more than once in the same
            % subject
            if size(find(x.ind == SplVox{n,i}(j)),1)>1
                blah = find(x.ind == SplVox{n,i}(j));
                x.test(i,:) = x.subspace(blah(1));
            else
                x.test(i,:) = x.subspace(x.ind == SplVox{n,i}(j),:)
            end
        end
    end
end
%} 
Err = RankError(XY,ind,k);
end