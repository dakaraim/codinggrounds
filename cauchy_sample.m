% Use Inverse CDF to generate 1000 samples from Y ~ Cauchy(0,2)
function x = cauchy_sample(m,d,n)
x = zeros(n,1);
for i = 1:n
    u = rand;
    x(i) = d*tan(pi*(u-0.5)) + m;
end
hist(x)
end
