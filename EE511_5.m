clc
clear
close all

%% DTFS MC Stationary Distributions: Find stationary distributions for a discrete time finite state (DTFS) Markov chain {X_n} having transition matrix: P

a = 1/10; b = 1/15;
P{1}  = [1-a , a; b, 1-b];

a = 0.5; b = 0.5;
P{2}  = [1-a , a; b, 1-b];

a = 1; b = 1;
P{3}  = [1-a , a; b, 1-b];

a = 0; b = 0;
P{4}  = [1-a , a; b, 1-b];

% Equilibrium distributions derived using Brouwers fixed point theorem
static.one = ([P{1}(3)/(P{1}(2) + P{1}(3)), P{1}(2)/(P{1}(2) + P{1}(3))])
static.two = ([P{2}(3)/(P{2}(2) + P{2}(3)), P{2}(2)/(P{2}(2) + P{2}(3))])
static.three = ([P{3}(3)/(P{3}(2) + P{3}(3)), P{3}(2)/(P{3}(2) + P{3}(3))])
static.four = ([P{4}(3)/(P{4}(2) + P{4}(3)), P{4}(2)/(P{4}(2) + P{4}(3))])

    
%% DTFS MC Simulation: Simulate 10 sample paths of length 500 for the DTFS Markov chains below using the update equation
t_0 = [1 0]; % initial state
P_one = [0 1; 1 0];
one = zeros(10,500,1,2);
for j = 1:10
    prob = rand(1,500);
    for i = 1:500; one(j,i,:,:) = t_0*P_one^i; 
    if prob(i) > one(j,i,1,1); state1(j,i) = 2; 
            else state1(j,i) = 1; end
    end
end

P_two = [0.75 00.25; 0.1 0.9];
two = zeros(10,500,1,2);
for j = 1:10
    prob = rand(1,500);
    for i = 1:500; two(j,i,:,:) = t_0*P_two^i; 
        if prob(i) > two(j,i,1,1); state2(j,i) = 2; 
            else state2(j,i) = 1; end 
    end
end

P_three = [0.48 0.48 0.04; 0.22, 0.7 0.08; 0 0 1];
t_3 = [1 0 0];
three = zeros(10,500,1,3);
for j = 1:10
    prob = rand(1,500);
    for i = 1:500; three(j,i,:,:) = t_3*P_three^i;     
        if three(j,i,1,1) > prob(i)        
            state3(j,i) = 1;     
        elseif three(j,i,1,1) < prob(i) < three(j,i,1,2)       
            state3(j,i) = 2;
        else state3(j,i) = 3; end 
    end
end


% Plotting Results
figure; for i = 1:size(one,1); stem(426:500,one(i,426:500,1,1)); hold on; stem(426:500,one(i,426:500,1,2)); hold on; end
xlabel('Path length'), ylabel('State probability'); legend('state 1','state 2')

figure; for i = 1:size(two,1); stem(426:500,two(i,426:500,1,1)); hold on; stem(426:500,two(i,426:500,1,2)); hold on; end
xlabel('Path length'), ylabel('State probability'); legend('state 1','state 2')

figure; for i = 1:size(three,1); stem(426:500,three(i,426:500,1,1)); hold on; stem(426:500,three(i,426:500,1,2)); hold on; stem(426:500,three(i,426:500,1,3)); hold on; end
xlabel('Path length'), ylabel('State probability'); legend('state 1','state 2', 'state 3')

% ensemble averages
ensemble.one = mean(one,1);
ensemble.two = mean(two,1);
ensemble.three = mean(three,1);

%Time averages
time.one = mean(one,2);
time.two = mean(two,2);
time.three = mean(three,2);

for i = 1:size(one,1)
    for j = 1:size(one,4)
[p.one(i,j) hyp.one(i,j) stats.one(i,j)] =  chi2gof(one(i,426:500,1,j));
    end
end

for i = 1:size(two,1)
    for j = 1:size(two,4)
[p.two(i,j) hyp.two(i,j) stats.two(i,j)] =  chi2gof(two(i,426:500,1,j));
    end
end

for i = 1:size(three,1)
    for j = 1:size(three,4)
[p.three(i,j) hyp.three(i,j) stats.three(i,j)] =  chi2gof(three(i,426:500,1,j));
    end
end

% P_one is not ergodic
tf1 = isergodic(dtmc(P_one))
% P_two is ergodic
tf2 = isergodic(dtmc(P_two))
% P_three is not ergodic
tf3 = isergodic(dtmc(P_three))