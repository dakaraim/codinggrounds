function smooth_pvcThick(array,smooth)
% Function will smooth the statistic displayed on the surface. You will
% need a separate function to display results on a smoothed 'flattened'
% surface
% INPUTS: array  - a cell array of subject base names 
%         smooth - a scalar or vector of integers.

for j = 1:length(smooth)
    ss = smooth(j);
    % Right Hemisphere
    p = parpool(6);
    parfor i = 1:length(array)
        pth=fileparts(array{i});
        % Check if PVC_Thickness files exist
        if exist(fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs'),'file')
            s=fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs');
            fprintf('Smoothing Subject %s \n',s);
            s1 = fullfile(pth,['atlas.pvc-thickness_0-6mm.smooth' num2str(ss) '.0mm.right.mid.cortex.dfs']);
            unix(['/home/dakaraim/BrainSuite19a/svreg/bin/svreg_smooth_surf_function.sh ', s, ' ', s , ' ', s1, ' ' num2str(ss)]);
        end
    end
    delete(p)
    
    % Left hemisphere
    p = parpool(6);
    parfor i = 1:length(array)
        pth=fileparts(array{i});
        % Check if PVC-Thickness files exist
        if exist(fullfile(pth,'atlas.pvc-thickness_0-6mm.left.mid.cortex.dfs'),'file')
            s=fullfile(pth,'atlas.pvc-thickness_0-6mm.left.mid.cortex.dfs');
            fprintf('Smoothing Subject %s \n',s);
            s1 = fullfile(pth,['atlas.pvc-thickness_0-6mm.smooth' num2str(ss) '.0mm.left.mid.cortex.dfs']);
            unix(['/home/dakaraim/BrainSuite19a/svreg/bin/svreg_smooth_surf_function.sh ', s, ' ', s , ' ', s1, ' ' num2str(ss)]);
        end
    end
    delete(p)
end

