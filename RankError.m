function Err = RankError(M,k)
%% Computes the error on a full data matrix, and on a single voxel in a subject
Err = struct('full',norm(M.data(M.ind,:) - M.data(M.ind,:)*M.V*M.V','fro')/norm(M.data(M.ind,:),'fro'));
Err.k = norm(M.data(M.ind,:) - M.data(M.ind,:)*M.V(:,1:k)*M.V(:,1:k)','fro')/norm(M.data(M.ind,:),'fro');

% Get a subject from a voxel
sv = randsample(M.ind,1);
Err.k_sub = norm(M.data(sv,:) - M.data(sv,:)*M.V(:,1:k)*M.V(:,1:k)','fro')/norm(M.data(sv,:),'fro');
Err.full_sub = norm(M.data(sv,:) - M.data(sv,:)*M.V*M.V','fro')/norm(M.data(sv,:),'fro');
end