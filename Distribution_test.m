function Distribution_test(stat, flag, k, n)
% Function will plot the distribution of a test statistic. 
% if flag = 1 it will plot the distribution of Hotelling Tsq statistic
% Tsq(n-k)/(k(n-1)) = F(k,n-k), n = nx + ny -1, k = number of features
% F

if flag == 1 % Plot F distribution
    val = 0:0.01:10;
    f = fpdf(val,k,n-k); %fpdf is a builtin matlab function
end
figure; 
subplot(2,2,[1,3])
    h=histogram(stat*(((n-k)/((n-1)*k))),k); 
    title(['Repeated measure of Tsq (' num2str(k) ', ' num2str(n-k) ')' num2str(numel(stat)),' times'])
subplot(2,2,[2,4])
    plot(val,f); %axis([0 5 0 1.3]); 
    title(['F-distrubution (' num2str(k) ', ' num2str(n-k) ')'])
    %axis([0 , ceil(max(stat*(((n-k)/((n-1)*k))))*10)/10  + .1 , 0, ceil(max(f)*10)/10])
