function AsspBiasCorrect(array, air_bin_dir, atlas_dir)
% Function will implement ASSP bias field correction
% INPUTS: array - a cell array with a variable string length per cell
%         air_bin_dir - AIR5.0 binary directory
%         atlas_dir   - directory for ASSP atlas

p = parpool(5);
parfor i = 1:length(array)
%for i = 1:length(array)
    if array{i} == ' '
        continue
    else
    disp(array{i})
    assp_correction(array{i},air_bin_dir,atlas_dir);
    end
end
 
delete(p)

end