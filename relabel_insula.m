clc;clear all;close all;restoredefaultpath;
addpath(genpath('/home/ajoshi/git_sandbox/svreg/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/3rdParty'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/MEX_Files'));

%% Run this only once when you want to create the new atlas
Create_Atlas=0;

if Create_Atlas
    
vl=load_nii_BIG_Lab('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.label.orig.nii.gz');


ins1=load_nii_BIG_Lab('/home/ajoshi/for_dakarai/dorsal_AI_bilateral_1mm_mapped2BCI.nii.gz');

ins2=load_nii_BIG_Lab('/home/ajoshi/for_dakarai/ventral_AI_bilateral_1mm_mapped2BCI.nii.gz');

ins3=load_nii_BIG_Lab('/home/ajoshi/for_dakarai/post_insula_bilateral_y-4_1mm_mapped2BCI.nii.gz');

msk=double((vl.img==500)|(vl.img==501));

vl.img = double(vl.img)+10*ins1.img.*msk + 20*ins2.img.*msk + 30*ins3.img.*msk;


save_untouch_nii(vl,'/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.label.nii.gz');

% Map the labels to surface and make it a new atlas
svreg_make_atlas('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain','/home/ajoshi/BrainSuite18a/svreg/BrainSuiteAtlas1/mri','-E');



% Generate screenshots
s1=readdfs('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.left.mid.cortex.dfs');

s1m=readdfs('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.left.mid.cortex_smooth10.dfs');
s1.vertices=s1m.vertices;

insmsk=(s1.labels>=500 & s1.labels<600);
s1.vcolor(insmsk==0,:)=.5;
figure;
patch('vertices',s1.vertices,'faces',s1.faces,'facevertexcdata',s1.vcolor,'edgecolor','none','facecolor','flat');material dull;axis equal;axis off;
view(-90,0);camlight;


s1=readdfs('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.right.mid.cortex.dfs');
s1m=readdfs('/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain.right.mid.cortex_smooth10.dfs');
s1.vertices=s1m.vertices;axis tight;

insmsk=(s1.labels>=500 & s1.labels<600);
s1.vcolor(insmsk==0,:)=.5;
figure;
patch('vertices',s1.vertices,'faces',s1.faces,'facevertexcdata',s1.vcolor,'edgecolor','none','facecolor','flat');material dull;axis equal;axis off;
view(90,0);camlight;axis tight;

end

%% Use the created atlas to label a subject
% Use the generated atlas to label a subject
svreg_labelwith_atlas('./405/anat/405_T1w', '/home/ajoshi/for_dakarai/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain', 'insula');

% Plot surface of subject
% Left hemisphere
s1=readdfs('/home/ajoshi/for_dakarai/405/anat/405_T1w.left.mid.cortex.svreg.insula.dfs');
s1m = smooth_cortex_fast(s1,.5,1000);
s1.vertices=s1m.vertices;

insmsk=(s1.labels>=500 & s1.labels<600);
s1.vcolor(insmsk==0,:)=.5;
h=figure;
patch('vertices',s1.vertices,'faces',s1.faces,'facevertexcdata',s1.vcolor,'edgecolor','none','facecolor','flat');material dull;axis equal;axis off;
view(-90,0);camlight;axis tight;
saveas(h,'405_left.png');

% Right hemisphere
s1=readdfs('/home/ajoshi/for_dakarai/405/anat/405_T1w.right.mid.cortex.svreg.insula.dfs');
s1m = smooth_cortex_fast(s1,.5,1000);
%s1m=readdfs('/home/ajoshi/for_dakarai/405/anat/405_T1w.right.mid.cortex_smooth10.dfs');
s1.vertices=s1m.vertices;axis tight;

insmsk=(s1.labels>=500 & s1.labels<600);
s1.vcolor(insmsk==0,:)=.5;
h=figure;
patch('vertices',s1.vertices,'faces',s1.faces,'facevertexcdata',s1.vcolor,'edgecolor','none','facecolor','flat');material dull;axis equal;axis off;
view(90,0);camlight;axis tight;
saveas(h,'405_right.png');

