% Use Inverse CDF to generate 1000 samples from X ~ exp(5) 
function x = exp_sample(theta, n)
x = zeros(n,1);
for i = 1:n
    u = rand; % generate random sample between [0-1]
    x(i) = -theta*log(1-u); % Get sample from inverse CDF
end
hist(x)
end
