function a = GetBaseNames(search_dir, search_criteria, filename)
% Function gets subject base names 
% OUTPUT:  a - an array of subject base names
% INPUT:   search_dir      - Directory in which to begin the search
%          search_criteria - String to search for in the name of the files
%          filename        - the desired filename should include file
%                            extension 


file = fullfile(search_dir,filename);
% Create file
current_dir = pwd;
cd(search_dir);
unix(['/home/dakaraim/matlab/GetSubBaseName.sh ', search_dir, ' ', search_criteria, ' ', file]);
cd(current_dir);

a={}; % initialize array
fid = fopen(file,'r');
ii=1; % initialize counter
while ~feof(fid)
    a{ii}= fscanf(fid, '%s',1); % save one line at a time
    ii=ii+1; %increase counter
end
fclose(fid);
end
