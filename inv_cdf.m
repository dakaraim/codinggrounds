function x = inv_cdf(n, p )
x = zeros(n,1);
for i = 1:n
    tmp = rand;
    if tmp <= p
        x(i) = 1;
    else
        continue
    end
end
hist(x)
end
