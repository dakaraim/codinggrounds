%% VectorizeThis
% Hotelling simulation
clear; clc; close all
% k = rank of reduced time series
V = 10000; L = V/2; k = 30;  T = 230; nx = 25; ny = 27;
% Construct matrix of normally distributed random numbers
rng(1)
x = struct('data',randn(T,nx,V)); 
y = struct('data',randn(T,ny,V));

% Randomly sample K vertices K times from population data
tic
Tsq= [];
[Tsq, Err, df,x,y] = Hotelling_Sim(x,y,L,k);
toc
% Plot distribution
% k = df.numerator, n = df.denominator+df.numerator -1 
Distribution_test(Tsq,1,df.numerator,df.denominator+df.numerator-1);
