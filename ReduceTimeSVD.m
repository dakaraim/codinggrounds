function x = ReduceTimeSVD(x,ind,subs,k)
%% x must be a structure, ind are the indices of interest, k is the reduced time dimension

% reduce time dimension in each subject
for i = 1:subs
    [x.u,x.w,x.v] = svd(x.data(ind,:,i),'econ');
    x.subspace(:,:,i) = x.data(ind,:,i)*x.v(:,1:k);
end
x.av = mean(x.subspace,3); % get average across subjects
% Compute square of distance from the mean in each voxel in each subject
for i = 1:subs
    for j = 1:size(ind,1)
        x.ss(j,:,:,i) = (x.subspace(j,:,i) - x.av(j,:))'*(x.subspace(j,:,i) - x.av(j,:));        
    end
end
% compute the sum of the squares in each voxel
for j = 1:size(ind,1)
    x.var(j,:,:) = sum(x.ss(j,:,:,:),4);
end