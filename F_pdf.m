function F = F_pdf(x,n,m)
 F = (n/m)^(n/2)*x.^(n/2 -1).*(1+(n/m)*x).^(-(n+m)/2)./(beta(n/2,m/2));
end
