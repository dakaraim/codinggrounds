clc;clear all;close all;restoredefaultpath;
addpath(genpath('/home/ajoshi/git_sandbox/svreg/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/3rdParty'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg/MEX_Files'));
pvc_dir = '/home/dakaraim/BrainSuite19a/svreg/bin/thicknessPVC.sh';

subbasename='/deneb_disk/for_dakarai/4_29/402_T1w_1m';

file = '/home/dakaraim/Data/MH_adolescent_waves/HandEdited/BrainSuite_Wave2_Processed/pvc_file_proc.txt';
a={}; % initialize array
fid = fopen(file,'r');
ii=1; % initialize counter
while ~feof(fid)
    a{ii}= fscanf(fid, '%s',1); % save one line at a time
    ii=ii+1; %increase counter
end
fclose(fid);
PVC_Thickness(a,pvc_dir);

for i = 1:length(a)
    generate_stats_xls(a{i});
end