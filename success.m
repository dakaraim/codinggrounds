% random variable to count the number of  success in 7 bernoulli trials
function y = success(n,p)
y = zeros(n,1);
for i = 1:n
    x = inv_cdf(7,p);
    y(i) = sum(x);
end
hist(y)
end