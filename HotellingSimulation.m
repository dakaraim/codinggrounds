function [stat,Err,df] = HotellingSimulation(x,y,L,k)
%% This code will run a hotelling simulation for two groups. 
% Stats SVD is used to reduce time dimension and is performed on each
% subject in each group indepdenently. 

[stat, df, XY,~,~] = HotellingsTest(x,y,L,k);
Err = RankError(XY,k);
Distribution_test(stat(:,1),1,df.numerator,df.denominator+df.numerator-1);
end
