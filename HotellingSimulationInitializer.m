%%Hotelling Simulation inttializer

%% Hotelling simulation 
% k = rank of reduced time series
V = 1000; L = 500;   T = 230; k = 30; nx = 25; ny = 27;
% Construct matrix of normally distributed random numbers
%rng(1)
x = struct('data',randn(V,T,nx));
y = struct('data',randn(V,T,ny));
rng(1)
for i = 1:1000
[Stat(i,:),Err(i),df(i)] = HotellingSimulation(x,y,L,k);
end
save('Tsq_1000_rng_1','Stat','Err','df')