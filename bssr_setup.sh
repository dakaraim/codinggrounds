#!/bin/bash


BaseDir=$PWD
statsdir="$PWD/bssr_wave2"
ReRun="$statsdir/Assp2.txt"
# Make BSSR output directory
if [[ -d $statsdir ]];
then
	rm -rf $statsdir
	echo "Directory '$statsdir' is removed"
fi

mkdir $statsdir 
echo "Directory '$statsdir' has been created."

# Get subject numbers
subjnum="$(find * -maxdepth 1 -type d -name '[0-9][0-9][0-9]')"

# change to directory of subject 
for sub in $subjnum
do
cd $BaseDir/$sub/anat
mkdir "$statsdir/$sub"
pvc=$(find * -iname 'atlas*.dfs')
    for f in $pvc
    do

	if [[ $(find * -iname '*smooth*.dfs'| wc -c) -gt 0 ]]; 
	# find returns zero if a file was found or not. To check if the file was found check to see if word count is zero.
	# Append ReRun file with subject directory and add a newline. 
	# Use backticks to evaluate $ReRun using command substitution.
	    then
		echo "Copying $f to $statsdir/$sub"
		cp  $f "$statsdir/$sub";
	    else        
		echo "Could not find atlas.pvc-thickness_0-6mm.hemi.cortex.dfs"
                echo "May need to rerun Assp.bfc"
		echo "Found $PWD/$(find * -iname atlas*.dfs) files."
		echo "Saving $PWD to $ReRun"
		echo $statsdir/$sub >> $ReRun
	fi
    done
svreg=$(find * -iname '*svreg*')
    for f in $svreg
    do 
        if [[ -e $f ]]
        then 
               cp  $f "$statsdir/$sub"
        fi  
    done
	
	cd $BaseDir
done



