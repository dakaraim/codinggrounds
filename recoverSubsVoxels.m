function [vox, sub] = recoverSubsVoxels(ind,V)
%% remember a/b = Q*b + R; ind/V = sub*V + vox
% V is the total number of voxels, and vox is the sampled voxel vector
[sub,vox] = quorem(sym(ind),sym(V));
sub = double(sub); vox = double(vox);
% test = sub*V + vox;
%isequal(test,ind);
end
