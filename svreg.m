function svreg(array, svreg_bin, svreg_atlas, flags )
% Function calls svreg to operate on a specific subject with a specific
% atlas. Flags modifies the svreg sequence

p= parpool(5);
parfor i = 1:length(array)
    %Check to make sure correct Atlas was used
    %{
     pth=fileparts(a{i});
     s.vertices=0;
     if exist(fullfile(pth,'atlas.right.mid.cortex.svreg.dfs'),'file')
        s=readdfs(fullfile(pth,'atlas.right.mid.cortex.svreg.dfs'));
     end
     
     if (length(s.vertices) ~= 349805)
         fprintf('Reprocessing SVReg Subject %s \n',a{i});
        unix(['/home/dakaraim/BrainSuite18a/svreg/bin/svreg.sh ', a{i}, ' /home/dakaraim/atlas/BCI-DNI_brain_atlas_dakarai/BCI-DNI_brain', ' -U -m']);
     end
    clear s
    %}
    
    unix([svreg_bin, ' ', array{i}, ' ', svreg_atlas, ' ',sprintf('-%s',flags)]);
end
delete(p);

end