%% VectorizeThis
% Hotelling simulation
clear; clc; close all
k = 20; V = 100; % k = rank of reduced time series
nx = 25; ny = 27;
x = randn(230,nx,V); 
y = randn(230,ny,V); 
Tsq = Hotelling(x,y,k);
    
function Tsq = Hotelling(x,y,k)

% hotellings student T test for one voxel
% This code performs a rank reduction on the voxels

% Create two vectors that are of size (Time, Vertices, number of subjects)

nx = size(x,2) ; ny = size(y,2); V = size(x,3);

if sum(isequal(mean(x,1), mean(y,1)) ,2) ~= 0
    disp("Error, Matrices cannot have the same mean")
    return
end

%Joint Rank Projector
XY = [x y];
ProjX = zeros(k,nx,V); ProjY = zeros(k,ny,V);
SSx = zeros(k,k,V); SSy = zeros(k,k,V);
Tsq = zeros(1,V);

% Compute SVD and choose k first values to reduce the rank.
for i = 1:V
    [Uxy(:,:,i),~,~] = svd(XY(:,:,i));
end

for i = 1:V
    ProjX(:,:,i) = Uxy(1:k,:,i)*x(:,:,i);
    ProjY(:,:,i) = Uxy(1:k,:,i)*y(:,:,i);
    
    % Compute sample covariance matrices
    SSx(:,:,i) = cov(ProjX(:,:,i)');
    SSy(:,:,i) = cov(ProjY(:,:,i)');
    
    
end

% mean across subjects
yBar = squeeze(mean(ProjX,2));
xBar = squeeze(mean(ProjY,2));


%{
% Diagonal components
for j = 1:k
   for i = 1:nx
    SSx(j,j) = (1/(nx-1))*sum(ProjX(i,j)-xBar(1,j)^2);
   end
end
%Need to figure out how to count these components!

for jj = 1:k
for jk = 1:k
    for ii = 1:nx
        SSxoff(jj,jk) = (1/(nx-1))*sum((ProjX(i,jj)-xBar(1,jj))*(ProjX(i,jk)-xBar(1,jk)));
    end
end
end
%}

% Compute common population covariance matrix
Spl = (SSx + SSy)/(nx+ny-2); %Spl = inv(Spl);
Spl = Spl*((1/nx) +(1/ny)); % scaling or t-squared statistic
for i = 1:V
    % Compute Tsquared statistic at every voxel (1,V)
    xyBar = (xBar(:,i) - yBar(:,i))';
    Tsq(:,i) = xyBar*(Spl(:,:,i)\xyBar');
end
end

