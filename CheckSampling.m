function x = CheckSampling(x)
% Finds the number of distinct subjects who sampled the same voxel more
% than once.
for i  = 1: length(unique(x.vox))
    if numel(unique(x.sub(x.vox == unique(x.vox(i))))) > 1
        x.collect(i) = unique(x.vox(i));
    end
end

