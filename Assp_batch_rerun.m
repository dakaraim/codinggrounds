clear all 
clc
close all

addpath(genpath('/home/dakaraim/Data/MH_adolescent_waves/'));
addpath(genpath('/home/dakaraim/matlab/'));
%Inputs - Get all subbasenames in an array
a={};
fid = fopen('wave2_basenames.txt','r');ii=1;
while ~feof(fid) 
a{ii}= fscanf(fid, '%s',1);ii=ii+1;
end
fclose(fid);

%% Run cortical Exraction with Assp corrected files. 
 p = parpool(10);
 parfor i = 1:length(a)
     unix(['/home/dakaraim/BrainSuite19a/bin/cortical_extraction_assp.sh ', a{i}]);
 end
delete(p)
 
%% Run svreg but just compute surface and use single threaded mode
p= parpool(10);
for i = 1:length(a)
     pth=fileparts(a{i});
     s.vertices=0;
     if exist(fullfile(pth,'atlas.right.mid.cortex.svreg.dfs'),'file')
        s=readdfs(fullfile(pth,'atlas.right.mid.cortex.svreg.dfs'));     
     end
     
     if (length(s.vertices) ~= 349805)
         fprintf('Reprocessing SVReg Subject %s \n',a{i});
        unix(['/home/dakaraim/BrainSuite19a/svreg/bin/svreg.sh ', a{i}, ' /home/dakaraim/BrainSuite19a/svreg/BCI-DNI_brain_atlas/BCI-DNI_brain', ' -S -U']);
     end
 end
 %delete(p);
 
 clear s
 %% Run Cortical thickness measurements using partial volume fractionation
 p = parpool(4);
 parfor i = 1:length(a)
     pth=fileparts(a{i});
     s.vertices=0;
     if exist(fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs'),'file')
        s=readdfs(fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs'));     
     end
     
     if (length(s.vertices) ~= 349805)     
        unix(['/home/dakaraim/BrainSuite19a/svreg/bin/thicknessPVC.sh ', a{i}]);
     end
 end
 delete(p)
 