% Create two vectors that are of size (Time, Vertices, number of subjects)
clear; clc;
% compute mean across subjects
N1 = 1; N2 = 1; T = 230; V = 96000;
x = rand(T,V,N1); xu = mean(x,1);
y = rand(T,V,N2); yu = mean(y,1);

if sum(isequal(xu,yu,2) ~= 0
    disp("Error, Matrices cannot have the same mean")
    return
end

SS1 = sum((y - yu).^2);

SS2 = sum((x - xu).^2);