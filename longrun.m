% the longest run of heads in n bernoulli trials

function y = longrun(n,p)
x = inv_cdf(n, p );
[~, len, ~, ~] = CountOnes(x);
y = max(len);
hist(y);
end

