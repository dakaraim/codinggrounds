function [out] = SumSquares(data,av,vox,subs)
% Function to compute sum of squares in each voxel
% x must be a structure with subject and average data

t = tic;
for i = 1:vox
  %  t = tic;
    for j = 1:subs
    SumSquare(j,:,:) = (data(:,i,j) - av(:,i))*(data(:,i,j) - av(:,i))';
    end
    out(i,:,:) = sum(SumSquare,1); % covariance matrix 
   % toc(t);
   % disp(i);    
end
toc(t);
%disp(i);
end
