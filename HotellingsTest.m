function [stat, df,XY,x,y] = HotellingsTest(x,y,L,k)
nx = size(x.data,3); ny = size(y.data,3); V = size(x.data,1); T = size(x.data,2);

% create joint matrix, concatenate across subjects
XY= struct('data',cat(3,x.data, y.data));

% randomly sample L indices from V so that we sample the same voxels in every subject
ind = randi(V,[L,1]); 

% Find x, y and joint matrix indices
x.ind = bsxfun(@times,ind,1:nx); x.ind = x.ind(:); 
y.ind = bsxfun(@times,ind,[1:ny] + nx); y.ind = y.ind(:);
XY.ind = bsxfun(@times,ind,1:(nx+ny)); XY.ind = XY.ind(:);

% Flatten matrix to be of dimension [V*(nx+ny) x T]. x and y should be dimension [V x T x n]
% before running the next command
XY.data = permute(XY.data,[2 1 3]);
XY.flat = reshape(XY.data,[T,(nx+ny)*V])';

% Compute SVD and rank reduce matrix
[XY.U,XY.S,XY.V] = svd(XY.flat(XY.ind,:),'econ');
x.subspace = XY.flat(x.ind,:)*XY.V(:,1:k);
y.subspace = XY.flat(y.ind,:)*XY.V(:,1:k);

% Compute mean in every voxel across subjects
x.sub = reshape(x.subspace', [k, L, nx]); x.av = mean(x.sub,3);
y.sub = reshape(y.subspace', [k, L, ny]); y.av = mean(y.sub,3);

% Compute sum of squares in every voxel
x = SumSquares(x,L,nx); 
y = SumSquares(y,L,ny);

df = struct('numerator',k);
df.denominator = nx + ny - 2; df.numerator = k;

% Loop over all the voxels.How to eliminate for loops here? 
for i = 1:L
    % Pooled variance
    Spl = (squeeze(x.var(i,:,:))*(nx-1) + squeeze(y.var(i,:,:))*(ny-1))./df.denominator;
    % sample covariacnce matrix
    Spl = Spl*((1/nx)+(1/ny));
    stat(i) = (x.av(:,i)-y.av(:,i))'*(Spl\(x.av(:,i)-y.av(:,i)));
end


