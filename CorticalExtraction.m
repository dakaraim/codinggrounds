function CorticalExtraction(array, executable)
% Function will run Cortical Extraction sequence 
% INPUTS: array - a cell array of basenames
%         executable - full path to script that run cortical extraction
%         sequence

p = parpool(5);
parfor i = 1:length(array)
    unix([executable ' ' array{i}]);
end
delete(p)

end