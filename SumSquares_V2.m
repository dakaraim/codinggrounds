function [out] = SumSquares_V2(x,vox,subs)
% Function to compute sum of squares in each voxel
% x must be a structure with subject and average data
out = zeros(vox,size(x.av,1),size(x.av,1));
for i = 1:vox
    aux = x.av(:,i);
    aux_2 = squeeze(x.data(:,i,:));
    for j = 1:subs
    SumSquare(:,:,j) = (aux_2(:,j) - aux)*(aux_2(:,j) - aux)';   
    end
    out(i,:,:) = sum(SumSquare,1);
end