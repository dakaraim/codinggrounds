clear all 
clc
close all

addpath(genpath('/home/dakaraim/Data/MH_adolescent_waves/'));
addpath(genpath('/home/dakaraim/matlab/'));
%Inputs - Get all subbasenames in an array
a={};
fid = fopen('assp.txt','r');ii=1;
while ~feof(fid) 
a{ii}= fscanf(fid, '%s',1);ii=ii+1;
end
fclose(fid);

% Run cortical Exraction 
%  p = parpool(10);
%  parfor i = 1:length(a)
%      unix(['/home/dakaraim/BrainSuite18a/bin/cortical_extraction_assp.sh ', a{i}]);
%  end
% delete(p)
%  
% Run svreg but just compute surface and use single threaded mode
 p= parpool(10);
 parfor i = 1:length(a)
     unix(['/home/dakaraim/BrainSuite18a/svreg/bin/svreg.sh ', a{i}, ' /home/dakaraim/BrainSuite18a/svreg/BCI-DNI_brain_atlas/BCI-DNI_brain', ' -S -U']);
 end
 delete(p);
 
 % Run Cortical thickness measurements using partial volume fractionation
 p = parpool(10);
 parfor i = 1:length(a)
     unix(['/home/dakaraim/BrainSuite18a/svreg/bin/thicknessPVC.sh ', a{i}]);
 end
 delete(p)