function PVC_Thickness(array, pvc_dir)
% Function calls PVC Thickness to calculate Grey Matter Thickness
% INPUTS: array - a cell array with variable full paths
%         pvc_dir - Directory to PVC_Thickness.sh executable.

p = parpool(5);
parfor i = 1:length(array)
    % Check to make sure correct atlas was used
    %{
     pth=fileparts(a{i});
     s.vertices=0;
     if exist(fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs'),'file')
        s=readdfs(fullfile(pth,'atlas.pvc-thickness_0-6mm.right.mid.cortex.dfs'));
     end
     
     if (length(s.vertices) ~= 349805)
        unix(['/home/dakaraim/BrainSuite18a/svreg/bin/thicknessPVC.sh ', a{i}]);
     end
    %}
    unix([pvc_dir,' ', array{i}]);
    
    % re-create the stats xls file. 
    %unix(['/home/dakaraim/BrainSuite19a/svreg/bin/generate_stats_xls.sh', ' ',array{i}]);
end
delete(p)
end